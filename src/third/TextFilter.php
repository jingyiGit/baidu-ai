<?php

namespace BaiduAi\third;

class TextFilter
{
  private static $rule = [
    [
      'type' => 'str',
      'text' => ['月', '日'],
    ],
    [
      'type' => 'str',
      'text' => ['(', '回复'],
    ],
    [
      'type' => 'str',
      'text' => ['（', '回复'],
    ],
    [
      'type' => 'str',
      'text' => ['（', '语音'],
    ],
    [
      'type' => 'preg',
      'text' => '#.*?\w{1,}\.\w{1,}\.\w{1,}.*?#',
    ],
  ];
  
  public static function filter($res)
  {
    $words = [];
    foreach ($res['words_result'] as $v) {
      if (isset($v['location']['height'])) {
        if ($v['location']['height'] >= 10) {
          $words[] = self::ruleFilter($v['words']);
        }
      } else {
        $words[] = self::ruleFilter($v['words']);
      }
    }
    return implode("\r\n", $words);
  }
  
  private static function ruleFilter($words)
  {
    // 长度小于2个的，抛弃
    if (strlen($words) < 2) {
      return '';
    }
    
    foreach (self::$rule as $v) {
      // 文本模式
      if ($v['type'] == 'str') {
        if (is_array($v['text'])) {
          $count = 0;
          foreach ($v['text'] as $text) {
            if (stripos($words, $text) !== false) {
              $count++;
            }
          }
          if ($count == count($v)) {
            return '';
          }
          
        } else if (stripos($words, $v['text']) !== false) {
          return '';
        }
        
        // 正则模式
      } else if ($v['type'] == 'preg') {
        if (preg_match($v['text'], $words, $vv)) {
          return '';
        }
      }
    }
    return self::ruleFixed($words);
  }
  
  /**
   * 一些固定的替换
   *
   * @param $words
   * @return array|string|string[]
   */
  private static function ruleFixed($words)
  {
    $words = str_replace(['86-1'], '1', $words);
    return str_replace([
      '⊙',
      '腾讯地图',
    ], '', $words);
  }
}
